declare const dotenv: any;
declare const model: any;
declare const initialize: (options: object | any) => any;
declare const connect: (ssl: object | any | null) => any;
